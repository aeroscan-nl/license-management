#!/bin/bash -l

set -e
[[ -z ${SETUP_CMD:-} ]] && set -uo pipefail

BUNDLE_JOBS="$(nproc)"
export BUNDLE_JOBS
export BUNDLE_WITHOUT="development:test"
export CI_API_V4_URL="${CI_API_V4_URL:-https://gitlab.com/api/v4}"
export CI_DEBUG_TRACE=${CI_DEBUG_TRACE:='false'}
export DOTNET_CLI_TELEMETRY_OPTOUT=1
export GO111MODULE=on
export GOPATH=${HOME}/.local
export HISTFILESIZE=0
export HISTSIZE=0
export LANG=C.UTF-8
export LICENSE_FINDER_CLI_OPTS=${LICENSE_FINDER_CLI_OPTS:=--no-debug}
export LM_REPORT_FILE=${LM_REPORT_FILE:-'gl-license-management-report.json'}
export MAVEN_CLI_OPTS="${MAVEN_CLI_OPTS:--DskipTests}"
export NO_UPDATE_NOTIFIER=true
export PIPENV_VENV_IN_PROJECT=1
export PREPARE="${PREPARE:---prepare-no-fail}"
export RECURSIVE='--no-recursive'
export RUBY_GC_HEAP_INIT_SLOTS=800000
export RUBY_GC_MALLOC_LIMIT=79000000
export RUBY_HEAP_FREE_MIN=100000
export RUBY_HEAP_SLOTS_GROWTH_FACTOR=1
export RUBY_HEAP_SLOTS_INCREMENT=400000

[[ $CI_DEBUG_TRACE == 'true' ]] && echo "$@"

project_dir="${CI_PROJECT_DIR:-${@: -1}}"
[[ -d $project_dir ]] && cd "$project_dir"

function debug_env() {
  pwd
  ls -alh
  env | sort
  asdf current

  ruby -v
  gem --version
  bundle --version
  bundle config

  python --version
  pip --version
}

function scan_project() {
  gem install --local -f --silent "$LM_HOME/pkg/*.gem"
  license_management ignored_groups add development
  license_management ignored_groups add develop
  license_management ignored_groups add test
  echo license_management report "$@"
  # shellcheck disable=SC2068
  license_management report $@
}

function prepare_dotnet() {
  [[ $(ls ./*.sln 2> /dev/null) ]] && RECURSIVE="--recursive"
}

function prepare_tools() {
  if ! asdf current 2> >(grep -q 'is not installed'); then
    echo "Installing missing tools…"
    asdf install
  fi
}

function prepare_project() {
  if [[ -z ${SETUP_CMD:-} ]]; then
    prepare_tools || true
    prepare_dotnet || true
  else
    echo "Running '${SETUP_CMD}' to install project dependencies…"
    # shellcheck disable=SC2068
    ${SETUP_CMD[@]}
    PREPARE="--no-prepare"
  fi
}

switch_to python "$(major_version_from "${LM_PYTHON_VERSION:-3}")"
switch_to java "adopt-openjdk-${LM_JAVA_VERSION:-8}"

prepare_project
[[ $CI_DEBUG_TRACE == 'true' ]] && debug_env

scan_project "$PREPARE" \
  --format=json \
  --save="${LM_REPORT_FILE}" \
  "$RECURSIVE" \
  "$LICENSE_FINDER_CLI_OPTS"
