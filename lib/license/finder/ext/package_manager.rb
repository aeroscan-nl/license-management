# frozen_string_literal: true

module LicenseFinder
  class PackageManager
    def current_packages_with_relations
      current_packages
    rescue StandardError => e
      ::License::Management.logger.error(e)
      raise e unless @prepare_no_fail

      []
    end
  end
end
