# frozen_string_literal: true

module LicenseFinder
  module SharedHelpers
    def shell
      ::License::Management.shell
    end

    class Cmd
      def self.run(command)
        ::License::Management.shell.execute(command)
      end
    end
  end
end
