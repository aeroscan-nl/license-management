# frozen_string_literal: true

require 'license/finder/ext/bower'
require 'license/finder/ext/conan'
require 'license/finder/ext/dependency'
require 'license/finder/ext/go_modules'
require 'license/finder/ext/gradle'
require 'license/finder/ext/license'
require 'license/finder/ext/maven'
require 'license/finder/ext/npm'
require 'license/finder/ext/nuget'
require 'license/finder/ext/package_manager'
require 'license/finder/ext/pip'
require 'license/finder/ext/pipenv'
require 'license/finder/ext/shared_helpers'
require 'license/finder/ext/yarn'

# Apply patch to the JsonReport found in the `license_finder` gem.
LicenseFinder::JsonReport.prepend(License::Management::Report)
