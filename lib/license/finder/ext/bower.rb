# frozen_string_literal: true

module LicenseFinder
  class Bower < PackageManager
    def prepare
      shell.execute([:bower, :install, '--allow-root'], env: default_env)
    end

    def current_packages
      map_all(bower_output).flatten.compact
    end

    def possible_package_paths
      [project_path.join('bower.json')]
    end

    private

    def bower_output
      stdout, _stderr, status = Dir.chdir(project_path) do
        shell.execute([:bower, :list, '--json', '-l', 'action', '--allow-root'])
      end
      return {} unless status.success?

      JSON.parse(stdout)
    end

    def map_all(modules)
      [map_from(modules)] +
        modules.fetch('dependencies', {}).values.map { |x| map_all(x) }
    end

    def map_from(bower_module)
      meta = bower_module.fetch('pkgMeta', {})
      endpoint = bower_module.fetch('endpoint', {})

      Dependency.new(
        'Bower',
        meta['name'] || endpoint['name'],
        meta['version'] || endpoint['target'],
        description: meta['readme'],
        detection_path: detected_package_path,
        homepage: meta['homepage'],
        install_path: bower_module['canonicalDir'],
        spec_licenses: Package.license_names_from_standard_spec(meta),
        summary: meta['description']
      )
    end

    def default_env
      return {} unless shell.custom_certificate_installed?

      {
        'NPM_CONFIG_CAFILE' => ENV.fetch('NPM_CONFIG_CAFILE', shell.custom_certificate_path.to_s),
        'bower_ca' => ENV.fetch('bower_ca', shell.custom_certificate_path.to_s)
      }
    end
  end
end
